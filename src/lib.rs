pub mod dict;
pub mod dictconfig;
pub mod styling;

#[cfg(test)]
mod tests {
    use super::dictconfig;
    fn modrym_collation_rules() -> dictconfig::CollationRules {
        // use smallvec::smallvec;
        let alpha_str = "mh m nh n ñh ñ ŋh ŋ p b t d k g ĥ f v þ ð s z š ž h ħ ŝ ẑ c č ṡ ż r j w l ḷ xh x řh ř ǧh ǧ i ü e ö a u o";
        let alpha: dictconfig::Alphabet = alpha_str
            .split_ascii_whitespace()
            .map(|s| vec![s.to_string()])
            .collect();
        dictconfig::CollationRules::new(true, &alpha[..])
    }
    #[test]
    fn test_collation_rules() {
        let collation = modrym_collation_rules();
        assert_eq!(collation.key_of("rümako"), vec![31, 43, 1, 46, 12, 48]);
        assert_eq!(collation.key_of("nhŋh"), vec![2, 6]);
    }
    #[test]
    fn test_alphabet_parsing() {
        use crate::dictconfig::parse_alphabet;
        let s = "a b c d efg";
        assert_eq!(
            parse_alphabet::parse_alpha_string(s),
            vec![vec!["a"], vec!["b"], vec!["c"], vec!["d"], vec!["efg"]]
        );
    }
    #[test]
    fn test_alphabet_parsing_escape() {
        use crate::dictconfig::parse_alphabet;
        let s = "\\[ \\\\ \\] \\ä";
        assert_eq!(
            parse_alphabet::parse_alpha_string(s),
            vec![vec!["["], vec!["\\"], vec!["]"], vec!["ä"]]
        );
    }
    #[test]
    fn test_alphabet_parsing_group() {
        use crate::dictconfig::parse_alphabet;
        let s = "a [b c] d";
        assert_eq!(
            parse_alphabet::parse_alpha_string(s),
            vec![vec!["a"], vec!["b", "c"], vec!["d"]]
        );
    }
    #[test]
    fn test_vkakx_plain() {
        use crate::dict::vkakx::*;
        let s = "hello";
        assert_eq!(
            parse_definition(&s),
            Ok(Definition {
                content: vec![Element::Text(Text {
                    content: "hello".to_string()
                })]
            })
        );
    }
}
