use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process::exit;

use derelict::dict;
use derelict::dictconfig;

const USAGE: &str = "
derelict: Compiles dictionaries from text
    <file> (string) input file name
";

#[derive(Default, Debug)]
struct Config {
    input_file: String,
}

impl Config {
    fn parse_args() -> Self {
        let args = lapp::parse_args(USAGE);
        let file = args.get_string("file");
        Config { input_file: file }
    }
}

fn main() {
    let config = Config::parse_args();
    let f = match File::open(&config.input_file[..]) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("{}: {}", &config.input_file[..], e);
            exit(-1);
        }
    };
    let mut lines = BufReader::new(f).lines();
    let mut option_str = String::new();
    let mut initial_lines = 0;
    loop {
        match lines.next() {
            Some(Ok(l)) => {
                initial_lines += 1;
                if l == "END" {
                    break;
                }
                option_str += &l;
                option_str += "\n";
            }
            Some(Err(e)) => {
                eprintln!("{}: {}", &config.input_file[..], e);
                exit(-1);
            }
            None => {
                eprintln!("{}: no `END` found", &config.input_file[..]);
                exit(-1);
            }
        }
    }
    let options = match dictconfig::options_from_toml(&option_str[..]) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{}", e);
            exit(-1);
        }
    };
    let lines_transform = lines.map(|res| match res {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{}", e);
            exit(-1);
        }
    });
    let mut entries = match dict::read_entries(&options, lines_transform) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{}", e.offset_lines(initial_lines));
            exit(-1);
        }
    };
    entries.sort_by(|e1, e2| {
        e1.key
            .cmp(&e2.key)
            .then(e1.name.cmp(&e2.name))
            .then(e1.pos.cmp(&e2.pos))
    });
    eprintln!("{:#?}", entries);
}
