use serde::Deserialize;
use std::collections::HashMap;
use std::fmt::{self, Write};

use crate::dict::vkakx;
use crate::dict::DictEntry;
use crate::dictconfig::Options;

#[derive(Debug, Deserialize)]
pub struct DefinitionStyle {
    newline: String,
    bold: String,
    italic: String,
    link: String,
    link_superscript: String,
    placeholder: String,
    escapes: HashMap<char, String>,
}

#[derive(Debug, Deserialize)]
pub struct Style {
    def_style: DefinitionStyle,
    alpha_section: String,
    section_head: String,
    part_of_speech: String,
    etymology: String,
    // index_title: String,
}

fn escape(s: &str, escapes: &HashMap<char, String>) -> String {
    let mut res = String::new();
    for c in s.chars() {
        match escapes.get(&c) {
            Some(s2) => res += s2,
            None => res.push(c),
        }
    }
    res
}

fn subst(template: &str, s: &str, escapes: &HashMap<char, String>) -> String {
    let mut res = String::new();
    for c in template.chars() {
        if c == '#' {
            res += &escape(s, escapes);
        } else {
            res.push(c);
        }
    }
    res
}

pub fn write_element(
    fh: &mut impl Write,
    elem: &vkakx::Element,
    style: &Style,
    options: &Options,
) -> Result<(), fmt::Error> {
    use vkakx::Element;
    match elem {
        Element::Newline => fh.write_str(&style.def_style.newline),
        Element::Text(t) => fh.write_str(&escape(&t.content, &style.def_style.escapes)),
        Element::Format(f) => {
            let mut s = String::new();
            write_def(&mut s, &f.content, style, options)?;
            let template = match f.kind {
                vkakx::FormatType::Bold => &style.def_style.bold,
                vkakx::FormatType::Italic => &style.def_style.italic,
            };
            fh.write_str(&subst(template, &s, &style.def_style.escapes))
        }
        Element::WordLink(l) => {
            let mut s = escape(&l.word, &style.def_style.escapes);
            if let Some(pos) = &l.pos {
                s += &subst(
                    &style.def_style.link_superscript,
                    pos,
                    &style.def_style.escapes,
                );
            }
            fh.write_str(&subst(&style.def_style.link, &s, &style.def_style.escapes))
        }
        Element::Placeholder(p) => fh.write_str(&subst(
            &style.def_style.placeholder,
            &p.symbol,
            &style.def_style.escapes,
        )),
    }
}

pub fn write_def(
    fh: &mut impl Write,
    def: &[vkakx::Element],
    style: &Style,
    options: &Options,
) -> Result<(), fmt::Error> {
    for elem in def {
        write_element(fh, elem, style, options)?;
    }
    Ok(())
}

pub fn write_entry(
    fh: &mut impl Write,
    entry: &DictEntry,
    style: &Style,
    options: &Options,
) -> Result<(), fmt::Error> {
    fh.write_str(&subst(
        &style.def_style.link,
        &entry.name,
        &style.def_style.escapes,
    ))?;
    fh.write_str(&subst(
        &style.part_of_speech,
        &options.parts_of_speech[entry.pos],
        &style.def_style.escapes,
    ))?;
    if let Some(etym) = &entry.etymology {
        fh.write_str(&subst(&style.etymology, etym, &style.def_style.escapes))?;
    }
    Ok(())
}

pub fn write_entries(
    fh: &mut impl Write,
    entries: &[DictEntry],
    style: &Style,
    options: &Options,
) -> Result<(), fmt::Error> {
    let cur_first: Option<usize> = None;
    fh.write_str(&style.section_head)?;
    for entry in entries {
        let first = entry.key.get(0);
        match (cur_first, first) {
            (Some(cur_first), Some(first)) if cur_first < *first => {
                let letter = options.collation.get_symbol(*first);
                fh.write_str(&subst(
                    &style.alpha_section,
                    &letter,
                    &style.def_style.escapes,
                ))?;
                fh.write_str(&style.section_head)?;
            }
            _ => (),
        }
        write_entry(fh, entry, style, options)?;
    }
    Ok(())
}
