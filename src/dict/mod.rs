use std::error::Error;
use std::fmt::{self, Display};
use std::iter::once;
use std::mem::swap;

pub mod vkakx;
use crate::dictconfig::Options;

#[derive(Debug)]
pub struct DictEntry {
    pub name: String,
    pub key: Vec<usize>,
    pub pos: usize, // index into options
    pub etymology: Option<String>,
    pub definition: vkakx::Definition,
}

#[derive(Debug, Clone)]
pub enum DictErrValue {
    PosRedeclared,
    NoName,
    NoDef,
    BadPos,
    DefinitionParse(vkakx::ParseErr),
}

impl Display for DictErrValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            DictErrValue::PosRedeclared => "Part of speech redeclared",
            DictErrValue::NoName => "Name not found",
            DictErrValue::NoDef => "Definition not found",
            DictErrValue::BadPos => "Bad part of speech",
            DictErrValue::DefinitionParse(e) => {
                return write!(f, "Error while parsing def: {:?}", e);
            }
        };
        write!(f, "{}", msg)
    }
}

#[derive(Debug, Clone)]
pub struct DictErr {
    lineno: usize,
    err: DictErrValue,
}

impl Display for DictErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error at line {}: ", self.lineno + 1)
    }
}

impl DictErr {
    pub fn new(lineno: usize, err: DictErrValue) -> DictErr {
        DictErr { lineno, err }
    }
    pub fn offset_lines(self, n: usize) -> DictErr {
        DictErr {
            lineno: self.lineno + n,
            err: self.err,
        }
    }
}

impl Error for DictErr {}

pub fn read_entries(
    opts: &Options,
    lines: impl Iterator<Item = String>,
) -> Result<Vec<DictEntry>, DictErr> {
    let mut entries: Vec<DictEntry> = vec![];
    let mut name: Option<String> = None;
    let mut pos: Option<usize> = None;
    let mut etymology: Option<String> = None;
    let mut definition: String = String::new();
    for (i, line) in lines.chain(once("#".to_string())).enumerate() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        let mut it = line.chars();
        let sigil = it.nth(0);
        it.next();
        let rest_line = it.as_str().trim();
        match &name {
            Some(the_name) => {
                // there is a name
                match sigil {
                    Some('#') => {
                        // Start a new entry
                        if definition.is_empty() {
                            return Err(DictErr::new(i, DictErrValue::NoDef));
                        }
                        let key = opts.collation.key_of(the_name);
                        let pos = match pos {
                            Some(p) => p,
                            None => return Err(DictErr::new(i, DictErrValue::BadPos)),
                        };
                        let mut etym2: Option<String> = None;
                        swap(&mut etymology, &mut etym2);
                        let parsed_def = match vkakx::parse_definition(&definition[..]) {
                            Ok(o) => o,
                            Err(e) => {
                                return Err(DictErr::new(i, DictErrValue::DefinitionParse(e)))
                            }
                        };
                        entries.push(DictEntry {
                            name: the_name.clone(),
                            key,
                            pos,
                            etymology: etym2,
                            definition: parsed_def,
                        });
                        definition = String::new();
                        name = Some(rest_line.to_string());
                    }
                    Some(':') => match opts.parts_of_speech.iter().position(|a| a == rest_line) {
                        Some(p) => pos = Some(p),
                        None => return Err(DictErr::new(i, DictErrValue::BadPos)),
                    },
                    Some('<') => {
                        etymology = Some(rest_line.to_string());
                    }
                    _ => {
                        if !definition.is_empty() {
                            definition += "\n";
                        }
                        definition += line;
                    }
                }
            }
            None => {
                // there is not yet a name
                match sigil {
                    Some('#') => {
                        name = Some(rest_line.to_string());
                    }
                    _ => {
                        return Err(DictErr::new(i, DictErrValue::NoName));
                    }
                }
            }
        }
    }
    Ok(entries)
}
