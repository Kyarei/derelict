use nom;
use nom::branch::alt;
use nom::bytes::complete::{escaped_transform, is_a, tag};
use nom::character::complete::{alphanumeric1, anychar, char, one_of, space1};
use nom::combinator::{complete, map, opt, verify};
use nom::error::ErrorKind;
use nom::multi::{many0, separated_list};
use nom::sequence::{delimited, pair, preceded};
use nom::IResult;

/*
Brief summary of the markup language:

* backslash escapes any non-alphanumeric character
* *asterisks* for bold, _underscores_ for italic
* @this or @{that syntax} to link to entries
* $I for placeholders (${MULTIPLE WORDS} supported)

*/

#[derive(Debug, Clone, PartialEq)]
pub struct Text {
    pub content: String,
}

#[derive(Debug, Clone, PartialEq)]
pub enum FormatType {
    Bold,
    Italic,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Format {
    pub kind: FormatType,
    pub content: Vec<Element>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct WordLink {
    pub word: String,
    pub pos: Option<String>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Placeholder {
    pub symbol: String,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Element {
    Text(Text),
    Format(Format),
    WordLink(WordLink),
    Placeholder(Placeholder),
    Newline,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Definition {
    pub content: Vec<Element>,
}

pub type ParseErr = nom::Err<(String, ErrorKind)>;

pub fn parse_definition(s: &str) -> Result<Definition, ParseErr> {
    /*fn escaped_char(s: &str) -> IResult<&str, char> {
        preceded(char('\\'), one_of(":\\*_@{}$"))(s)
    }*/
    // Nasty hack to forbid empty strings from parsing
    // so many0 won't throw a fit.
    fn plain_text(s: &str) -> IResult<&str, String> {
        verify(
            escaped_transform(alphanumeric1, '\\', anychar),
            |s: &String| !s.is_empty(),
        )(s)
    }
    fn plain_text_or_space(s: &str) -> IResult<&str, String> {
        verify(
            escaped_transform(alt((alphanumeric1, is_a(" ,./-–:"))), '\\', anychar),
            |s: &String| !s.is_empty(),
        )(s)
    }
    fn parse_text(s: &str) -> IResult<&str, Text> {
        map(plain_text_or_space, |t| Text { content: t })(s)
    }
    fn pos(s: &str) -> IResult<&str, Option<String>> {
        opt(preceded(char(':'), plain_text))(s)
    }
    fn parse_link(s: &str) -> IResult<&str, WordLink> {
        map(
            alt((
                preceded(char('@'), pair(plain_text, pos)),
                delimited(tag("@{"), pair(plain_text_or_space, pos), char('}')),
            )),
            |(word, pos)| WordLink { word, pos },
        )(s)
    }
    fn parse_placeholder(s: &str) -> IResult<&str, Placeholder> {
        map(
            alt((
                preceded(char('$'), plain_text),
                delimited(tag("${"), plain_text, char('}')),
            )),
            |symbol| Placeholder { symbol },
        )(s)
    }
    fn parse_bold(s: &str) -> IResult<&str, Format> {
        map(delimited(char('*'), parse_elements, char('*')), |es| {
            Format {
                kind: FormatType::Bold,
                content: es,
            }
        })(s)
    }
    fn parse_italic(s: &str) -> IResult<&str, Format> {
        map(delimited(char('_'), parse_elements, char('_')), |es| {
            Format {
                kind: FormatType::Italic,
                content: es,
            }
        })(s)
    }
    fn parse_formatted(s: &str) -> IResult<&str, Format> {
        alt((parse_bold, parse_italic))(s)
    }
    fn parse_element(s: &str) -> IResult<&str, Element> {
        alt((
            map(parse_text, |t| Element::Text(t)),
            map(char('\n'), |_| Element::Newline),
            map(parse_formatted, |f| Element::Format(f)),
            map(parse_link, |l| Element::WordLink(l)),
            map(parse_placeholder, |p| Element::Placeholder(p)),
        ))(s)
    }
    fn parse_elements(s: &str) -> IResult<&str, Vec<Element>> {
        many0(parse_element)(s)
    }
    fn parse_top(s: &str) -> IResult<&str, Definition> {
        map(complete(parse_elements), |es| Definition { content: es })(s)
    }
    match parse_top(s) {
        Ok((rem, res)) => {
            if rem.is_empty() {
                Ok(res)
            } else {
                Err(nom::Err::Error((rem.to_string(), ErrorKind::Eof)))
            }
        }
        Err(nom::Err::Incomplete(n)) => Err(nom::Err::Incomplete(n)),
        Err(nom::Err::Error((r, k))) => Err(nom::Err::Error((r.to_string(), k))),
        Err(nom::Err::Failure((r, k))) => Err(nom::Err::Failure((r.to_string(), k))),
    }
}
