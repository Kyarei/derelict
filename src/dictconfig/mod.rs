use radix_trie::Trie;
use serde::Deserialize;
use smallvec::SmallVec;
use toml::de;

pub mod parse_alphabet;
use parse_alphabet::parse_alpha_string;

fn true_fn() -> bool {
    true
}

#[derive(Debug, Deserialize)]
#[serde(from = "CollationFormat")]
pub struct CollationRules {
    case_sensitive: bool,
    alphabet: Trie<String, (usize, usize)>,
    alphabet_array: Alphabet,
}

type AlphaEntry = Vec<String>;
// type AlphaEntry<'a> = SmallVec<[&'a str; 4]>;
pub type Alphabet = Vec<AlphaEntry>;
pub type AlphabetSlice = [AlphaEntry];

impl CollationRules {
    pub fn new(case_sensitive: bool, alphabet: Alphabet) -> CollationRules {
        let mut alphabet_as_trie = Trie::<String, (usize, usize)>::new();
        for (i, letters) in alphabet.iter().enumerate() {
            for letter in letters.iter() {
                let l: String = if case_sensitive {
                    letter.clone()
                } else {
                    letter.to_lowercase()
                };
                let len = l.len();
                alphabet_as_trie.insert(l, (i, len));
            }
        }
        CollationRules {
            case_sensitive,
            alphabet: alphabet_as_trie,
            alphabet_array: alphabet,
        }
    }
    // Get the key of the given string.
    // If case_sensitive is false, then the string must be lowercased
    // ahead of time.
    pub fn key_of(&self, s: &str) -> Vec<usize> {
        let mut ss: &str = s;
        let mut key: Vec<usize> = vec![];
        while !ss.is_empty() {
            match self.alphabet.get_ancestor_value(ss) {
                Some((key_val, length)) => {
                    key.push(*key_val);
                    ss = &ss[*length..];
                }
                None => {
                    let mut it = s.chars();
                    it.next();
                    ss = it.as_str();
                }
            }
        }
        key
    }
    pub fn get_symbol(&self, i: usize) -> &str {
        match self.alphabet_array[i].get(0) {
            Some(s) => &s,
            None => "·",
        }
    }
}

#[derive(Deserialize)]
struct CollationFormat {
    #[serde(default = "true_fn")]
    case_sensitive: bool,
    alphabet: String,
}

impl From<CollationFormat> for CollationRules {
    fn from(f: CollationFormat) -> CollationRules {
        let alphabet_array: Alphabet = parse_alpha_string(f.alphabet.as_str());
        CollationRules::new(f.case_sensitive, alphabet_array)
    }
}

#[derive(Debug, Deserialize)]
pub struct Options {
    pub collation: CollationRules,
    pub parts_of_speech: Vec<String>,
}

pub fn options_from_toml(s: &str) -> Result<Options, de::Error> {
    toml::from_str(s)
}
