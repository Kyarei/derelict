use crate::dictconfig::*;

use nom::branch::alt;
use nom::bytes::complete::escaped_transform;
use nom::character::complete::{alphanumeric1, anychar, char};
use nom::combinator::{complete, map};
use nom::multi::separated_list;
use nom::sequence::delimited;
use nom::IResult;
use smallvec::{smallvec, SmallVec};

pub fn parse_alpha_string(s: &str) -> Alphabet {
    fn letter(s: &str) -> IResult<&str, String> {
        escaped_transform(alphanumeric1, '\\', anychar)(s)
    }
    // named!(escaped_char<&str, char>, preceded!(char!('\\'), anychar));
    // named!(letter_element<&str, char>, alt!(escaped_char | call!(|c: char| c.is_alphanum())));
    // named!(letter<&str, &str>, take_while1!(alt!(escaped_char | is_alphanumeric)));
    fn letters(s: &str) -> IResult<&str, Vec<String>> {
        separated_list(char(' '), letter)(s)
    }
    fn bracketed(s: &str) -> IResult<&str, Vec<String>> {
        delimited(char('['), letters, char(']'))(s)
    }
    fn top(s: &str) -> IResult<&str, Vec<Vec<String>>> {
        complete(separated_list(
            char(' '),
            alt((bracketed, map(letter, |l| vec![l]))),
        ))(s)
    }
    match top(s) {
        Ok((_, res)) => res,
        Err(e) => {
            eprintln!("{:?}", e);
            vec![]
        }
    }
}
